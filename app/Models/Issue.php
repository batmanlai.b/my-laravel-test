<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Magazine;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Issue extends Model
{
    use HasFactory;
    protected $table='issues';
    protected $guarded=[];

    public function magazine(): BelongsTo
    {
        return $this->belongsTo(Magazine::class);
    }
}

