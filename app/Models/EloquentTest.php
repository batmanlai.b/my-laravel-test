<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EloquentTest extends Model
{
    use HasFactory;
    protected $table='eloquent_tests';
    protected $guarded=[];
}
