<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;
use Modules\Auth2fa\Facades\TwoFactorAuth;
use Modules\Managers\Http\Resources\ManagerResource;
use Modules\ReviewApp\Entities\Account;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    public function index()
    {
        $users= User::all();
        return JsonResource::collection($users);
    }
    public function count()
    {
        $users = User::all()->count();
        return [
            'count'=>$users,
        ];
    }
    public function screener(User $user)
    {
        return new JsonResource($user);
    }
    public function init()
    {
        logger()->info("Admin panel is opened");

        return response()->json([
            'dl' => config('app.dl'),
        ]);
    }
    public function login()
    {
        request()->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);

        /** @var User $account */
        $account = User::where('email', request('email'))->first();

        if (is_null($account) || ! Hash::check(request('password'), $account->password)) {
            abort(422, 'Invalid email or password.');
        }

        return response()->json([
            'access_token' => retry(3, fn () => $this->createToken($account), 2)->accessToken,
            'token_type' => 'Bearer',
        ]);
    }
    private function createToken(User $account)
    {
        return $account->createToken('review-app');
    }
    public function logout()
    {
        optional(auth('review')->user()->token())->revoke();

        return response()->json(['message' => 'Successfully logged out.']);
    }
}
