<?php

namespace App\Http\Controllers;

use App\Models\Magazine;
use App\Models\Issue;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;


class MagazineController extends Controller
{
    public function index()
    {
        $magazines = Magazine::all();
        return JsonResource::collection($magazines);
    }
    public function count()
    {
        $magazines=Magazine::all()->count();
        return [
            'count'=>$magazines,
        ];
    }
}
