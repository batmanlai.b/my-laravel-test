<?php

namespace App\Http\Controllers;

use App\Models\Issue;
use App\Models\Magazine;
use http\Message;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class IssueController extends Controller
{
    public function index()
    {
        $issues = Issue::all();
        return JsonResource::collection($issues);
    }
    public function magazineIssues(Magazine $magazine)
    {
        return JsonResource::collection($magazine->issues()->get());
    }

    public function create()
    {
        Issue::create(request()->all());
    }

    public function update(Issue $issue)
    {
        $attributes = request()->validate([
            'title' => 'string|required',
            'price' => 'integer',
            'magazine_id' => 'integer',
        ]);
        $issue->update($attributes);
    }

    public function delete(Issue $issues)
    {
        $issues->delete();
    }
    public function count()
    {
        $issues=Issue::all()->count();
        return [
            'count' => $issues
        ];
    }
}
