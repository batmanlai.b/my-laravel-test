<?php

namespace App\Http\Controllers;

use App\Models\EloquentTest;
use Illuminate\Http\Request;

class EloquentTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EloquentTest  $eloquentTest
     * @return \Illuminate\Http\Response
     */
    public function show(EloquentTest $eloquentTest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EloquentTest  $eloquentTest
     * @return \Illuminate\Http\Response
     */
    public function edit(EloquentTest $eloquentTest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EloquentTest  $eloquentTest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EloquentTest $eloquentTest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EloquentTest  $eloquentTest
     * @return \Illuminate\Http\Response
     */
    public function destroy(EloquentTest $eloquentTest)
    {
        //
    }
    public function elo(Request $request)
    {
        $test = EloquentTest::query();
        if ($request->get('price')){
            $test->where('price', '<=', $request->get('price'));
        }

        return $test->get();
    }
}
