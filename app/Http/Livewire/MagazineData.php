<?php

namespace App\Http\Livewire;

use App\Models\Magazine;
use Livewire\Component;

class MagazineData extends Component
{
    public function render()
    {
        return view('livewire.magazine-data');
    }
}
