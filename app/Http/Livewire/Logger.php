<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Logger extends Component
{
    public function render()
    {
            return view('livewire.logger');
    }
}
