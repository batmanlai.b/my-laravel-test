<?php

namespace App\Http\Livewire;

use App\Jobs\UserUpdate;
use Illuminate\Http\Request;
use Livewire\Component;
use App\Models\User;
use App\Events\UsernameUpdated;

class EditUser extends Component
{
    public $name;
    public $updated=false;
    public function render()
    {
        return view('livewire.edit-user');
    }
    public function change()
    {
        $this->validate([
           'name' => 'required|string',
        ]);
        dispatch(new UserUpdate(auth()->user(),$this->name));
        $this->updated=true;
    }
}
