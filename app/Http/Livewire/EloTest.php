<?php

namespace App\Http\Livewire;

use App\Models\EloquentTest;
use Illuminate\Http\Request;
use Livewire\Component;

class EloTest extends Component
{
    public $price;
    public $test;
    public function render()
    {
        return view('livewire.elo-test');
    }
    public function change(Request $request)
    {
        $this->validate([
            'price' => 'required|integer',
        ]);
        $test = EloquentTest::query();
        if ($request->get('price')){
            $test->where('price', '<=', $request->get('price'));
        }
        return $test->get();
    }
    public function elo(Request $request)
    {
        $test = EloquentTest::query();
        if ($request->get('price')){
            $test->where('price', '<=', $request->get('price'));
        }

        return $test->get();
    }
}
