<?php

use App\Http\Controllers\IssueController;
use App\Http\Controllers\MagazineController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EloquentTestController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->prefix('v1')->group(function()
{
    Route::get('/user', function (Request $request){
        return $request->user();
    });
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
Route::get('/test',function (Request $request)
{
   return 'Successfully Signed in';
});

Route::get('/issues', [IssueController::class, 'index']);
Route::put('/issues/{issue}', [IssueController::class, 'update']);
Route::post('/issues/post', [IssueController::class, 'create']);
Route::delete('/issues/delete/{issues}', [IssueController::class, 'delete']);
Route::get('/issues/count', [IssueController::class, 'count']);
Route::get('/magazine/count', [MagazineController::class, 'count']);
Route::get('/user/count', [UserController::class, 'count']);
Route::get('/user/display', [UserController::class, 'index']);
Route::get('/magazine/index', [MagazineController::class, 'index']);
Route::get('/issues/index', [IssueController::class, 'index']);
Route::get('/magazine/{magazine}',[IssueController::class, 'magazineIssues']);
Route::get('/users/{user}', [UserController::class,'screener']);
Route::get('/panel/init', [UserController::class,'init']);
Route::post('/login', [UserController::class,'login']);
Route::delete('/logout',[UserController::class,'logout']);
