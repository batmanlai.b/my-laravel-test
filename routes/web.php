<?php

use App\Http\Middleware\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IssueController;
use App\Http\Controllers\EloquentTestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|http://localhost/
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/index',  );
Route::get('/henderman', [App\Http\Controllers\HomeController::class, 'no'])->middleware(Test::class);
Route::post('/upload', function (Request $request){
    dd($request->file("thing")->store('','google'));
});
Route::get('/elmundo', [\App\Http\Livewire\EloTest::class, 'render']);
