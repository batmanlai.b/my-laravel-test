export default {
    state: {
        user: null,
    },

    getters: {
        signedIn: state => state.user !== null,
    },

    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },
    },

    actions: {
        async loadUser({ commit }) {
            let response = await window.axios.get("http://localhost:8000/api/user/display");
            commit("SET_USER", response.data.data.user);
        }
    }
};
