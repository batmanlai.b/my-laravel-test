import axios from "axios";
import Errors from "./Errors";
import FailedResponseHandler from "./FailedResponseHandler";

class Form {
    /**
     * Create a new Form instance.
     *
     * @param {object} data
     */
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            if (field === "data") {
                throw "Form.js - `data` field is reserved for the form.";
            }

            this[field] = data[field];
        }

        this.errors = new Errors();
        this.processing = false;
        this._files = null;
        this._files2 = {};
    }

    /**
     * Fetch all relevant data for the form.
     */
    data() {
        if (this._files || Object.keys(this._files2).length > 0) {
            return this.makeFormData();
        }

        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
    }

    /**
     *
     */
    makeFormData() {
        let formData = new FormData();

        for (let property in this.originalData) {
            if (Array.isArray(this[property])) {
                this[property].forEach(value => {
                    formData.append(property + "[]", value);
                });
            } else {
                formData.append(property, this[property]);
            }
        }

        for (let i in this._files) {
            if (typeof this._files[i] === "object") {
                formData.append("attachments[]", this._files[i]);
            }
        }

        for (let fieldName in this._files2) {
            if (typeof this._files2[fieldName] === "object") {
                formData.append(fieldName, this._files2[fieldName]);
            }
        }

        return formData;
    }

    /**
     * Submit process take at least given duration.
     */
    takeAtLeast(duration) {
        this.takeAtLeastValue = duration;

        return this;
    }

    changed() {
        for (let field in this.originalData) {
            if (this[field] !== this.originalData[field]) {
                return true;
            }
        }

        return false;
    }

    /**
     * Reset the form fields.
     */
    reset() {
        for (let field in this.originalData) {
            this[field] = "";
        }

        this.errors.clear();
    }

    /**
     * Send a GET request to the given URL.
     * .
     * @param {string} url
     */
    get(url) {
        return this.submit("get", url);
    }

    /**
     * Send a POST request to the given URL.
     * .
     * @param {string} url
     */
    post(url) {
        return this.submit("post", url);
    }

    /**
     * Send a PUT request to the given URL.
     * .
     * @param {string} url
     */
    put(url, overrides = {}) {
        return this.submit("put", url, overrides);
    }

    /**
     * Send a PATCH request to the given URL.
     * .
     * @param {string} url
     */
    patch(url) {
        return this.submit("patch", url);
    }

    /**
     * Send a DELETE request to the given URL.
     * .
     * @param {string} url
     */
    delete(url) {
        return this.submit("delete", url);
    }

    /**
     * Submit the form.
     *
     * @param {string} requestType
     * @param {string} url
     */
    submit(requestType, url, overrides = {}) {
        return new Promise((resolve, reject) => {
            this.processing = true;
            this.errors.clear();

            let data = Object.assign(this.data(), overrides);

            let options = requestType === "delete" ? { params: data } : data;

            axios[requestType](url, options)
                .takeAtLeast(this.takeAtLeastValue)
                .then(response => {
                    this.onSuccess(response.data);

                    this.processing = false;

                    resolve(response.data);
                })
                .catch(error => {
                    if (error.response) {
                        this.onFail(error.response.data);
                    }

                    this.processing = false;

                    new FailedResponseHandler(error).handle();

                    reject(error.response ? error.response.data : null);
                });
        });
    }

    /**
     * Handle a successful form submission.
     *
     * @param {object} data
     */
    onSuccess() {}

    /**
     * Attach
     */
    attach(files) {
        this._files = files;

        return this;
    }

    /**
     * Put a file to the form.
     */
    withFile(fieldName, file) {
        this._files2[fieldName] = file;

        return this;
    }

    /**
     * Handle a failed form submission.
     *
     * @param {object} errors
     */
    onFail(errors) {
        this.errors.record(errors.errors, errors.message);
    }

    axios() {
        return axios;
    }
}

export default Form;
