import Errors from "./Errors";

class FailedResponseHandler {
    /**
     * Construct FailedResponseHandler.
     *
     * @param error
     */
    constructor(error) {
        this.error = error;
    }

    /**
     * Handle the error.
     */
    handle() {
        if (this.getErrorCode() === 422) {
            let errors = new Errors().record(
                this.error.response.data.errors,
                this.error.response.data.message
            );

            window.app.$store.dispatch("notifyErrors", errors);
        } else {
            window.app.$buefy.toast.open({
                duration: 3000,
                message: `Aлдаа: [${this.getErrorCode()}] ${this.getMessage()}`,
                position: "is-top",
                type: "is-danger"
            });
        }
    }

    /**
     * @returns {number}
     */
    getErrorCode() {
        if (this.error.response) {
            return this.error.response.status;
        }
    }

    /**
     * @returns {number}
     */
    getMessage() {
        if (this.error.response) {
            return this.error.response.data.message;
        }
    }
}

export default FailedResponseHandler;
