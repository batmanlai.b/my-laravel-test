import Vue from 'vue';
import Vuex from "vuex";
import VueTailwind from 'vue-tailwind';
import App from './App.vue';
import router from './router';
import store from './store';
import 'buefy';
import './index.css';
import './dist/tailwind.css';
import './../resources/js/bootstrap';
import Login from './modules/admin/pages/adses/Form/Login'

Vue.use(VueTailwind,Vuex);
window.app = new Vue({
    router,
    store,
    render: (h) => h(App),
    mounted() {
        this.init();
    },
    methods:{
      init(){
          this.$store
              .dispatch("loadUser")
              .then(() => this.$store.dispatch("initAdminPanel"))
              .catch(this.login);
      },
        login() {
            this.$buefy.modal.open({
                component: Login,
                props: {
                    whenLoggedIn: () => this.init()
                }
            });
        }
    },
}).$mount('#app');
