import Vue from 'vue';
import Vuex from "vuex";
Vue.use(Vuex);
import auth from "./auth";
export default new Vuex.Store({
    modules:{
        auth
    },
    state: {
        errors: null,
        translations: {},
        download_method: null,
        sales: {
            enabled: false
        },
        isReady: false,
        activeServices: []
    },

    getters: {

        serviceIsActive: state => serviceName => {
            return state.activeServices.includes(serviceName);
        }
    },

    mutations: {
        SET_ERRORS(state, errors) {
            state.errors = errors;
        },

        UNSET_ERRORS(state) {
            state.errors = null;
        },

        SET_TRANSLATIONS(state, translations) {
            state.translations = translations;
        },

        SET_DOWNLOAD_METHOD(state, download_method) {
            state.download_method = download_method;
        },

        SET_SALES(state, sales) {
            state.sales = sales;
        },

        SET_ACTIVE_SERVICES(state, services) {
            state.activeServices = services;
        },

        initialized(state) {
            state.isReady = true;
        },
    },

    actions: {
        async initAdminPanel({ commit }) {
            let response = await window.axios.get("http://localhost:8000/api/panel/init");
            commit("SET_DOWNLOAD_METHOD", response.data.download_method);
            commit("SET_SALES", response.data.sales);
            commit("SET_ACTIVE_SERVICES", response.data.active_services);
            commit("initialized");
        }
    }
});
