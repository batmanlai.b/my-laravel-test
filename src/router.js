import Vue from "vue";
import App from './App';
import VueRouter from "vue-router";

Vue.use(VueRouter);

let routes = [
    {
        path: "/collections",
        component: () => import("./modules/admin/pages/adses/Collection"),
    },
    {
        path: "/",
        component: () => import("./modules/admin/pages/adses/Dashboard"),
    },
    {
        path: "/feedback",
        component: () => import("./modules/admin/pages/adses/Feedback"),
    },
    {
        path: "/users",
        component: () => import("./modules/admin/pages/adses/Users"),
    },
    {
        path: "/users/:id",
        component: () => import("./modules/admin/pages/adses/User/UserList")
    },
    {
        path: "/collections/:id/issues",
        component: () => import("./modules/admin/pages/adses/Issues")
    },
    {
        path: "/dashboard",
        component: () => import("./modules/admin/pages/adses/Dashboard"),
    }
];
const router = new VueRouter
({
    mode: 'history',
        routes
})
new Vue({
    render: h => h(App),
    router,
}).$mount('#app')
export default router;
