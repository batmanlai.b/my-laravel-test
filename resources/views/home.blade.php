<!DOCTYPE html>
<html>
<head>
    <title>VueJS</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
<div class="bg-red-200 h-screen w-screen" id="app">
    <articles></articles>
    <example-component></example-component>
</div>
</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<script src="{{ asset('/js/app.js') }}"></script>
</html>

