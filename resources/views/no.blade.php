<div>
    <br><br><br>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="/upload" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="file" class="form-control" name="thing">
                    <br>
                    <input type="submit" class="btn btn-sm btn-block btn-danger" value="Upload">
                </form>
            </div>
        </div>
    </div>
</div>
