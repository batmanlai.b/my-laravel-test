<form>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Edit Name :</label>
                <input type="text" wire:model="name" class="form-control">
            </div>
        </div>
        <div class="col-md-12 text-center">
            <button class="btn text-white btn-success" wire:click.prevent="change">edit</button>
        </div>
    </div>
    <script>
        @if($updated==true)
            window.location.reload();
        @endif
    </script>
</form>
