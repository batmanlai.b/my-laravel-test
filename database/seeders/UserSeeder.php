<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
            ->insert(
                [
                    'name' => 'Mivazuko',
                    'email' => 'admin@wplus.world',
                    'email_verified_at' => null,
                    'password' => bcrypt('pass'),
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );
    }
}
