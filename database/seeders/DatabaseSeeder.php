<?php

namespace Database\Seeders;

use Database\Factories\TodoFactory;
use Faker\Core\Number;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Int_;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MagazineSeeder::class,
            IssuesSeeder::class,
            UserSeeder::class,
            ]);
    }
}
