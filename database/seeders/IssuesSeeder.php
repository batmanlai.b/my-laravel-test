<?php

namespace Database\Seeders;

use Faker\Core\Number;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Type\Integer;
use App\Models\Magazine;

class IssuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<50;$i++) {
            DB::table('issues')->insert(
                [
                    'title' => Str::random(9),
                    'release_date' => now(),
                    'magazine_id' => Magazine::all()->random()->id,
                    'price' => random_int(400, 5500),
                    'created_at' => now(),
                    'updated_at' => now(),
                ]
            );
        }

    }
}
