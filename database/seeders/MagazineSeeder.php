<?php

namespace Database\Seeders;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use function Sodium\increment;

class MagazineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10; $i++) {
            DB::table('magazine')
                ->insert(
                    [
                        'name' => Str::random(9),
                        'language' => Str::random(3),
                        'created_at' => now(),
                        'updated_at' => now(),
                    ]
                );
        }
    }
}
