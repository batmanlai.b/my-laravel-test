<?php

namespace Database\Factories;

use App\Models\Issue;
use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class IssuesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Issue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title'=>$this->faker->title(),
            'price'=>$this->faker->numerify(),
            'magazine_id'=>$this->faker->foreign()->randomDigitNotNull(),
        ];
    }
}
